// Usage: <a href="" class="hover" data-title="...">...</a>
$(".hover").mouseover(function(){
    $(this).animate({"opacity": 0}, 500, function(){
        var name = $(this).attr("data-title");
        $(this).text("Visit the \"" + name + "\" Thread");
    }).animate({"opacity": 1}, 500);
});
$(".hover").mouseleave(function() {
    $(this).animate({"opacity": 0}, 500, function(){
        $(this).text("Visit this Thread");
    }).animate({"opacity": 1}, 500);
});
